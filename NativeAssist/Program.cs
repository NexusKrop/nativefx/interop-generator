﻿// See https://aka.ms/new-console-template for more information
using CommandLine;
using NativeAssist;
using NativeAssist.CLI;
using NativeAssist.Generators;
using NativeRefHelper.Models;
using System.Diagnostics;
using System.Text.Json;
var l = Util.Logger;

l.Information("Starting new NativeAssist");

return await Parser.Default.ParseArguments<ClassicOptions, SplitOptions>(args)
    .MapResult(
        (ClassicOptions classic) => Interface.RunClassic(classic),
        (SplitOptions split) => Interface.RunSplit(split),
        errs => Task.FromResult(0)
        );