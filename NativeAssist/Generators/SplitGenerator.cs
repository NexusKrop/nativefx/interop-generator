﻿namespace NativeAssist.Generators;

using NativeAssist.CLI;
using NativeAssist.Properties;
using NativeRefHelper.Models;
using System;
using System.Collections.Generic;

internal partial class SplitGenerator
{
    private readonly string _targetDir;
    private readonly SplitOptions _options;
    private readonly Dictionary<string, Dictionary<string, NativeFunction>> natives;

    public SplitGenerator(Dictionary<string, Dictionary<string, NativeFunction>> natives,
        SplitOptions options)
    {
        _options = options;
        _targetDir = _options.OutDir;
        this.natives = natives;
    }

    public void Initialize()
    {
        Util.Logger.Information("Initializing split generator");
        var escapeWords = Resources.EClassicEscapedWords.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var split in escapeWords)
        {
            _escapedWords.Add(split);
        }

        var aliases = Resources.EClassicScrHandleAliases.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var split in aliases)
        {
            _scrHandleAliases.Add(split);
        }
    }

    public void Run()
    {
        Directory.CreateDirectory(_targetDir);
        var version = Network.GetVersion();

        foreach (var ns in natives)
        {
            var nsKey = ns.Key.PascalCase();
            Util.Logger.Information("Processing namespace {NsKey}", nsKey);

            using var writer = new StreamWriter(File.Create(Path.Combine(_targetDir, $"Natives.{nsKey}.cs")));
            WriteHeader(writer, version);
            writer.WriteLine($"public static class {nsKey} {{");

            foreach (var native in ns.Value)
            {
                this.OperateNative(writer, native.Value, native.Key, nsKey);
            }

            writer.WriteLine("}}");
        }
    }

    public void WriteHeader(TextWriter writer, string version)
    {
        writer.WriteLine(Resources.FileHeader.Replace("$version$", version)
            .Replace("$ns$", _options.Namespace)
            .Replace("$class$", _options.ClassName));
    }
}
